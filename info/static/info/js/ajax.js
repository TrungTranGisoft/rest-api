function showSnippet(str) {
    let xhttp;
    let url = 'http://127.0.0.1:8000/snippets/' + String(str) +'/';

    if (!str) {
        return
    };

    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById('jsonresp-snippets').innerHTML = this.responseText;
        }
    };

    xhttp.open("GET", url, true);
    xhttp.send();
}

function showUser(str) {
    let xhttp;
    let url = 'http://127.0.0.1:8000/snippets/users/' + String(str) +'/';

    if (!str) {
        return
    };

    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById('jsonresp-users').innerHTML = this.responseText;
        }
    };

    xhttp.open("GET", url, true);
    xhttp.send();
}