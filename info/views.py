from django.shortcuts import render

def info(request):
    context = {}
    return render(request, 'info/index.html', context)