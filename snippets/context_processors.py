from django.contrib.auth.models import User

from .models import Snippet

def context(request):
    return {
        'snippets': Snippet.objects.all(),
        'users': User.objects.all()
    }